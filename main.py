import json
import sqlite3
from registration import *
from read import *
from fastapi import FastAPI, HTTPException
import uvicorn

'''@app.put('',
         summary='',
         tags=[''])'''

connection = sqlite3.connect("./database.db", check_same_thread=False)
client = connection.cursor()
app = FastAPI(title="PROGRAD students program")


@app.post('/students/new-student',
          summary='Add new student',
          tags=['Students'],
          )
def new_student(student_data: dict):
    return new_student_module(student_data)


@app.post('/graduation/new-graduation',
          summary='Add new graduation',
          tags=['Graduations'])
def new_graduation(graduation_data: dict):
    new_graduation_module(graduation_data)


@app.post('/teachers/new-teacher',
          summary='Adds new teacher',
          tags=['Teachers'])
def new_teacher(teacher_data: dict):
    new_teacher_module(teacher_data)


@app.post('/subjects/new-subject',
          summary='Adds new subject',
          tags=['Subjects'],
          )
def new_subject(subject_data: dict):
    new_teacher_module(subject_data)


@app.put('/students/modify-students-graduation',
         summary='Modify students graduation',
         tags=['Students'])
def modify_students_graduation(student_data: dict):
    modify_student_graduation_module(student_data)


@app.post('/students/enroll-student',
          summary='Enroll students into a subject',
          tags=['Students'])
def enroll_student(student_data: dict):
    enroll_student_module(student_data)


@app.delete('/students/unenroll-student',
            summary='Unenroll student',
            tags=['Students'])
def unenroll_student(student_data: dict):
    unenroll_student_module(student_data)


@app.put('/teachers/change-teacher-responsible',
         summary='Change the teacher responsible for the subject',
         tags=['Teachers'])
def change_teacher_responsible(subject_teacher_data: dict):
    change_teacher_responsible_module(subject_teacher_data)


#   from read


@app.get('/students/catch-students',
         summary='Catch all students',
         tags=['Students'])
def rescue_all_students():
    rescue_all_students_module()


@app.get('/students/catch-student/{id}',
         summary='Catch single student data',
         tags=['Students'])
def rescue_student(id: str):
    rescue_student_module(id)


@app.get('/students/show-students-in-graduation',
         summary='Show the students that belongs to a specific graduation',
         tags=['Students'])
def students_in_graduation(graduation_id: str):
    students_in_graduation_module(graduation_id)

@app.get('/students/show-students-in-subject',
         summary='Show the students that belongs to a specific subject',
         tags=['Students'])
def students_in_subject(subject_code: str):
    students_in_subject_module(subject_code)

@app.get('/subjects/ranking',
         summary='Show a ranking of subjects',
         tags=['Subjects'])
def ranking_of_subjects():
    ranking_of_subjects_module()


if __name__ == '__main__':
    uvicorn.run(app='main:app', port=3000, reload=True)
