import sqlite3
connection = sqlite3.connect("./database.db", check_same_thread=False)
client = connection.cursor()


def get_student_id(student_cpf):
    student = client.execute(f'Select id FROM alunos WHERE cpf = "{student_cpf}"')
    student = client.fetchall()
    if student:
        student = student[0][0]
        return student
    else:
        print("Aluno não cadastrado.")
        return None


def get_subject_id(subject_code):
    subject = client.execute(f'Select id FROM disciplinas WHERE codigo = "{subject_code}"')
    subject = client.fetchall()
    if subject:
        subject = subject[0][0]
        return subject
    else:
        print("Digite um código válido.")
        return None


def get_teacher_id(teacher_cpf):
    teacher = client.execute(f'Select professor_id FROM professores WHERE cpf = "{teacher_cpf}"')
    teacher = client.fetchall()
    if teacher:
        teacher = teacher[0][0]
        return teacher
    else:
        print("Professor não cadastrado, tente novamente.")
        return None


def get_graduation_id(graduation_name):
    graduation = client.execute(f'Select id FROM cursos WHERE nome = "{graduation_name}"')
    graduation = client.fetchall()
    if graduation != []:
        graduation = graduation[0][0]
        return graduation
    else:
        print("Curso não cadastrado, tente novamente.")
        return None
