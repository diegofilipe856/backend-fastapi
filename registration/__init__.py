import sqlite3
from uuid import uuid4
from data_manipulation import *
from fastapi import FastAPI, HTTPException
import uvicorn

'''@app.put('',
         summary='',
         tags=[''])'''

connection = sqlite3.connect("./database.db", check_same_thread=False)
client = connection.cursor()


def new_student_module(student_data):
    if student_data['cpf']:
        student = student_data['name']
        cpf = student_data['cpf']
        age = student_data['age']
        rg = student_data['rg']
        dispatching_agency = student_data['dispatching_agency']
        birthday = student_data['birthday']
        student_id = str(uuid4())
        student_graduation_id = student_data['student_graduation_id']
        db_rg = f'"{rg}"' if rg is not None else "NULL"
        db_dispatching_agency = f'"{dispatching_agency}"' if dispatching_agency is not None else "NULL"
        client.execute(
            f'INSERT INTO alunos values("{student_id}", "{student}", "{cpf}", {age}, {db_rg}, {db_dispatching_agency}, "{birthday}", "{student_graduation_id}");')
        connection.commit()
        return "Operação realizada com sucesso!"


def new_graduation_module(graduation_data):
    graduation_name = graduation_data['name']
    year_of_creation = graduation_data['year_of_creation']
    coordinator = graduation_data['coordinator']
    verify_coordinator = client.execute(f'SELECT * FROM professores  WHERE cpf = "{coordinator}"')
    verify_coordinator = client.fetchall()
    if verify_coordinator != []:
        building_name = graduation_data['building_name']
        graduation_id = str(uuid4())
        client.execute(
            f'INSERT INTO cursos values("{graduation_id}", "{graduation_name}", {year_of_creation}, "{verify_coordinator[0][0]}", "{building_name}");')
        connection.commit()
        return "Operação realizada com sucesso!"
    else:
        return "O coordenador não é um professor registrado. Tente novamente."


def new_teacher_module(teacher_data):
    name_of_teacher = teacher_data['name']
    cpf_teacher = teacher_data['cpf']
    if cpf_teacher:
        titulation = teacher_data['titulation']
        teacher_id = str(uuid4())

        client.execute(
            f'INSERT INTO professores values("{teacher_id}", "{name_of_teacher}", "{cpf_teacher}", "{titulation}");')
        connection.commit()
        return "Operação realizada com sucesso!"
    else:
        return "CPF obrigatório."


def new_subject_module(subject_data):
    subject_code = subject_data['code']
    subject_name = subject_data['name']
    teacher_cpf = subject_data['teacher_cpf']
    teacher_id = get_teacher_id(teacher_cpf)
    verify_teacher = client.execute(f'SELECT * FROM professores WHERE cpf = "{teacher_cpf}"')
    verify_teacher = client.fetchall()
    if verify_teacher:
        description = subject_data['description']
        subject_id = str(uuid4())
        client.execute(
            f'INSERT INTO disciplinas values("{subject_id}", "{subject_code}", "{subject_name}", "{teacher_id}", "{description}");')
        connection.commit()
        return "Operação realizada com sucesso!"


def modify_student_graduation_module(student_data):
    chosen_student = student_data['cpf']
    updated_graduation = student_data['updated_graduation']
    client.execute(f'SELECT id FROM cursos WHERE id = "{updated_graduation}"')
    updated_graduation_id = client.fetchall()
    if updated_graduation_id:
        updated_graduation_id = updated_graduation_id[0][0]
        client.execute(f'UPDATE alunos SET curso_id = "{updated_graduation_id}" WHERE cpf = "{chosen_student}";')
        connection.commit()
        return "Operação realizada com sucesso!"
    else:
        return "Graduação não cadastrada."


def enroll_student_module(student_data):
    student_cpf = student_data['cpf']
    student_id = get_student_id(student_cpf)
    if student_id is not None:
        subject = student_data['subject_code']
        subject_id = get_subject_id(subject)
        if subject_id is not None:
            client.execute(f'INSERT INTO aluno_disciplina VALUES("{subject_id}", "{student_id}");')
            connection.commit()
            return "Operação realizada com sucesso!"


def unenroll_student_module(student_data):
    student_cpf = student_data['cpf']
    student_id = get_student_id(student_cpf)
    if student_id is not None:
        subject = student_data['subject_code']
        subject_id = get_subject_id(subject)
        if subject_id is not None:
            client.execute(
                f'DELETE FROM aluno_disciplina WHERE disciplina_id = "{subject_id}" AND aluno_id = "{student_id}";')
            connection.commit()
            return "Operação realizada com sucesso!"


def change_teacher_responsible_module(subject_teacher_data):
    subject = subject_teacher_data['subject_code']
    subject_id = get_subject_id(subject)
    if subject_id is not None:
        teacher_to_replace = subject_teacher_data['teacher_cpf']
        teacher_id = get_teacher_id(teacher_to_replace)
        if teacher_id is not None:
            client.execute(f'UPDATE disciplinas SET professor = "{teacher_id}" WHERE id = "{subject_id}"')
            connection.commit()
            return "Operação realizada com sucesso!"


